<?php

namespace LaravelSiteErrors\Models;


use Illuminate\Database\Eloquent\Model;

class SiteErrors extends Model{

	const LEVEL_INFO = "info";
	const LEVEL_WARNING = "warning";
	const LEVEL_ERROR = "error";
	const LEVEL_ERROR_CRITICAL = "critical";

	const STATUS_NEW = "new";
	const STATUS_FIXED = "fixed";
}