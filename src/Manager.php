<?php


namespace LaravelSiteErrors;

use LaravelSiteErrors\Interfaces\IServiceInterface;
use LaravelSiteErrors\Models\SiteErrors;

class Manager{

	public static function check(IServiceInterface $service, $toDB = true):bool{

		$result = $service->handle();

		if($toDB && $result === false){

			$model = new SiteErrors();

			$errorCode = $service->getErrorCode();
			$name = $service->getName();
			$level = $service->getLevel();
			$object_id = $service->getObjectID();
			$pageUrl = $service->getPageUrl();
			$comment = $service->getComment();
			$status = SiteErrors::STATUS_NEW;

			$exist = $model->where("error_code", $errorCode)
			               ->where("name", $name)
			               ->where("level", $level)
			               ->where("object_id", $object_id)
			               ->where("page_url", $pageUrl)
			               ->where("comment", $comment)
			               ->where("status", $status)
			               ->exists();

			if(!$exist){

				$model->error_code = $errorCode;
				$model->name = $name;
				$model->level = $level;
				$model->object_id = $object_id;
				$model->page_url = $pageUrl;
				$model->comment = $comment;
				$model->status = $status;

				$model->save();
			}
		}

		return $result;
	}
}