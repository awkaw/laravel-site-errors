<?php

namespace LaravelSiteErrors\Helpers;


class JsonHelper{

	public static function decodeOrNull($string){

		$result = json_decode($string, true);

		if(json_last_error() === JSON_ERROR_NONE){
			return $result;
		}

		return null;
	}
}