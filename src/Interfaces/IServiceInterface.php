<?php

namespace LaravelSiteErrors\Interfaces;


interface IServiceInterface{

	public function getErrorCode():string;
	public function getLevel():string;
	public function setLevel($level);
	public function getName():string;
	public function getComment():string;
	public function handle():bool;

	public function setPageUrl($pageUrl);
	public function getPageUrl():string;
	public function setObjectID($objectID);
	public function getObjectID();
}