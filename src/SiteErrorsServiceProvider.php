<?php

namespace LaravelSiteErrors;


use Illuminate\Support\ServiceProvider;

class SiteErrorsServiceProvider extends ServiceProvider{

	public function boot()
	{
		$this->publishes([
			__DIR__.'/../config/site_errors.php' => config_path('site_errors.php'),
		]);

		$this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
		$this->loadMigrationsFrom(__DIR__.'/../migrations');
	}

	public function register()
	{
		$this->mergeConfigFrom(__DIR__.'/../config/site_errors.php', 'site_errors');
	}
}