<?php


namespace LaravelSiteErrors\Services;


use Illuminate\Support\Facades\Log;
use LaravelSiteErrors\Helpers\JsonHelper;
use LaravelSiteErrors\Interfaces\IServiceInterface;
use LaravelSiteErrors\Models\SiteErrors;

class UrlResponseCodeService extends BaseService implements IServiceInterface{

	protected $responseCode = 0;

	public function getErrorCode(): string{
		return "url_response_code";
	}

	public function getName(): string{
		return "Ошибка ссылки на странице";
	}

	public function getComment(): string{
		return "Сервер вернул код ".$this->responseCode;
	}

	public function getLevel(): string{

		if(!is_null($this->level)){
			return $this->level;
		}

		return SiteErrors::LEVEL_ERROR;
	}

	public function handle(): bool{

		try{

			$response = false;

			if( $curl = curl_init() ) {

				curl_setopt($curl, CURLOPT_URL, $this->getObjectID());
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

				$response = curl_exec($curl);
				$this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

				curl_close($curl);
			}

			if($response === false){
				return false;
			}

			$this->setResponse($response);

			if($this->responseCode != 200){

				if($this->responseCode < 300){
					$this->level = SiteErrors::LEVEL_INFO;
				}else if($this->responseCode < 400){
					$this->level = SiteErrors::LEVEL_WARNING;
				}else if($this->responseCode < 500){
					$this->level = SiteErrors::LEVEL_ERROR;
				}else if($this->responseCode > 499){
					$this->level = SiteErrors::LEVEL_ERROR_CRITICAL;
				}

			}else{
				$this->level = SiteErrors::LEVEL_INFO;
			}

			return ($this->responseCode == 200);

		}catch(\Exception $e){

			Log::debug($e->getMessage()." in line ".$e->getLine()." in file ".$e->getFile());
			Log::debug($e->getTraceAsString());
		}

		return false;
	}
}
