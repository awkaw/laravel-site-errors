<?php


namespace LaravelSiteErrors\Services;


use LaravelSiteErrors\Interfaces\IServiceInterface;

class BaseService implements IServiceInterface{

	protected $objectID;
	protected $pageUrl;
	protected $level;
	protected $response;

	public function setPageUrl($pageUrl){
		$this->pageUrl = $pageUrl;
	}

	public function getPageUrl():string{
		return $this->pageUrl;
	}

	public function setObjectID($objectID){
		$this->objectID = $objectID;
	}

	public function getObjectID(){
		return $this->objectID;
	}

	public function handle():bool{
		return false;
	}

	public function getResponse(){
		return $this->response;
	}

	public function setResponse($response){
		$this->response = $response;
	}

	public function getComment(): string{
		return "";
	}

	public function getErrorCode(): string{
		return "no_code";
	}

	public function getLevel(): string{

		if(!is_null($this->level)){
			return $this->level;
		}

		return "no_level";
	}

	public function setLevel($level){
		$this->level = $level;
	}

	public function getName(): string{
		return "no_name";
	}
}