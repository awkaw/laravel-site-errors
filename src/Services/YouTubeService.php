<?php

namespace LaravelSiteErrors\Services;

use Illuminate\Support\Facades\Log;
use LaravelSiteErrors\Helpers\JsonHelper;
use LaravelSiteErrors\Interfaces\IServiceInterface;
use LaravelSiteErrors\Models\SiteErrors;

class YouTubeService extends BaseService implements IServiceInterface{

	public function getErrorCode(): string{
		return "youtube_video_failed";
	}

	public function getName(): string{
		return "Ошибка youtube ролика";
	}

	public function getLevel(): string{

		if(!is_null($this->level)){
			return $this->level;
		}

		return SiteErrors::LEVEL_WARNING;
	}

	public function getComment(): string{
		return "http://www.youtube.com/watch?v=".$this->getObjectID();
	}

	public function handle():bool{

		$result = $this->checkYoutubeVideo($this->getObjectID());

		return ($result !== false && !is_null($result) && is_array($result));
	}

	protected function checkYoutubeVideo($youtubeID){

		try{

			$url = "https://www.youtube.com/oembed?url=http://www.youtube.com/watch?v={$youtubeID}&format=json";

			$response = false;

			if( $curl = curl_init() ) {

				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

				$response = curl_exec($curl);

				curl_close($curl);
			}

			if($response === false){
				return false;
			}

			$this->setResponse($response);

			return JsonHelper::decodeOrNull($response);

		}catch(\Exception $e){

			Log::debug($e->getMessage()." in line ".$e->getLine()." in file ".$e->getFile());
			Log::debug($e->getTraceAsString());
		}

		return false;
	}
}