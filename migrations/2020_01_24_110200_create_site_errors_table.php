<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_errors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("error_code", 50)->index();
            $table->string("name")->index();
            $table->string("level")->index();
            $table->string("object_id", 200)->nullable()->index();
            $table->string("page_url", 200)->nullable();
            $table->text("comment")->nullable();
            $table->string("status", 20)->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_errors');
    }
}
